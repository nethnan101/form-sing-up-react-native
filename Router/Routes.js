import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { Homepage } from '../components/firstbody/Homepage';
import { Register } from '../components/firstbody/Register';
import Login from '../components/firstbody/Login';

export const Routes = () => {
    return (
        <Router>
            <Scene key='root'>
                <Scene key='home' component={Homepage} title ="Home" initial = {true} />
                <Scene key='register' component={Register} title='Register' />
                <Scene key='login' component={Login} title='Log In' />
            </Scene>
        </Router>
    )
}
