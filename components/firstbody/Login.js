import React, { useState } from 'react';
import { View, Text, Button, SafeAreaView, TextInput, StyleSheet, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

const Login = () => {
    const goBack = () => {
        Actions.register()
    }
    const [gmail, setGmail] = React.useState('');
    const [password, setPassword] = React.useState('')
    return (
        <View>
            <Text style={style.text}>Sign In</Text>
            <SafeAreaView>
                <TextInput style={style.input} onChangeText={setGmail} value={gmail} placeholder="Gmail"/>
                <TextInput style={style.input} secureTextEntry onChangeText={setPassword} value={password} placeholder="Password"/>
            </SafeAreaView>
            <View style={style.view}>
                <Button color='#fff' onPress={() => Alert.alert('Success!')} title='Log In' />
            </View>
            <Text style={style.textor}>OR</Text>
            <View style={style.viewtwo}>
                <Button color='#0EC5FF' onPress={() => Alert.alert('Please connect to your Facebook!')} title='Login with Facebook' />
            </View>
        </View>
    )
}
const style = StyleSheet.create({
    input:{
        top: 30,
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 30,
        fontSize: 18,
        borderColor: '#95E0F7'
    },
    text: {
        fontSize: 30,
        padding: 10
    },
    view: {
        top: 50,
        backgroundColor: '#DE583D',
        borderRadius: 30,
        width: '50%',
        alignSelf: 'center',
        borderWidth: 2,
        borderColor: '#fff'
    },
    textor:{
        top: 60,
        textAlign: 'center'
    },
    viewtwo: {
        top: 70,
        backgroundColor: '#fff',
        borderRadius: 30,
        width: '70%',
        alignSelf: 'center',
        borderColor: '#0EC5FF',
        borderWidth: 1
    }
})
export default Login
