import React, { useState } from 'react'
import { View, Text, Button, StyleSheet, TextInput, SafeAreaView, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

export const Register = () => {
    const goBack = () => {
        Actions.login()
    }
    const [text, setText] = React.useState('');
    const [name, setName] = React.useState('');
    const [phone, setPhone] = React.useState('');
    const [gmail, setGmail] = React.useState('');
    const [password, setPassword] = React.useState('')
    const [number, setNumber] = React.useState(null)
    
    return (
        <View style={style.container}>
            <Text style={style.text}>Create new account</Text>
            <SafeAreaView>
                <TextInput 
                    style={style.input} 
                    onChangeText={setName} 
                    value={name} 
                    placeholder="Full Name"
                    />
                <TextInput 
                    style={style.input} 
                    keyboardType="numeric" 
                    onChangeText={setPhone} 
                    value={phone} 
                    placeholder="Phone Number"
                    />
                <TextInput 
                    style={style.input} 
                    onChangeText={setGmail} 
                    value={gmail} 
                    placeholder="Gmail"
                    />
                <TextInput 
                    style={style.input} 
                    secureTextEntry 
                    returnKeyType='save' 
                    onChangeText={setPassword} 
                    value={password} 
                    placeholder="Password"
                    />
                {/* <Button 
                    title='Save'
                /> */}
            </SafeAreaView>
            <View style={style.signup}>
                <Button onPress={() => Alert.alert('Thank you for register!')} color='#fff' title='Sign Up' />
            </View>
            <Text style={style.ortext}>OR</Text>
            <View style={style.button}>
                <Button onPress={goBack} color='#DE583D' title='Log In' />
            </View>
        </View>
    )
}
const style = StyleSheet.create({
    container: {
        backgroundColor: '#F9ECE9',
        height: '100%'
    },
    text: {
        top: 20,
        textAlign: 'center',
        fontSize: 28,
        fontWeight: 'bold',
        color: '#D8472A', 
    },
    input:{
        top: 30,
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 30,
        fontSize: 18,
        borderColor: '#95E0F7'
    },
    button:{
        top: 70,
        position: 'relative',
        backgroundColor: '#fff',
        borderRadius: 30,
        width: '50%',
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#6FD9FB'
    }, 
    signup: {
        top: 50,
        position: 'relative',
        backgroundColor: '#DE583D',
        borderRadius: 30,
        width: '50%',
        alignSelf: 'center',
        borderColor: '#fff',
        borderWidth: 2
    },
    ortext:{
        top: 60,
        textAlign: 'center',        
    }
})
