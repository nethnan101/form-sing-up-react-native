import React from 'react';
import { SafeAreaView, SceneView, View, Text, StyleSheet, Button, Alert, StyleProp } from 'react-native';
import { Actions } from 'react-native-router-flux';

export const ButtonAdd = () => {
    const goToRegister = () => {
        Actions.register();
    }
    const goToLogin = () => {
        Actions.login();
    }
    return (
        <View>
            <Text style={style.text}></Text>
            <View onPress={goToLogin} style={style.view}>
                <Button
                    color="#fff"
                    style={style.buttonbutton}
                    title='Log In'
                    onPress={goToLogin}
                    />
            </View>
            <Text></Text>
            <View onPress={goToRegister} style={style.viewtwo}>
                <Button
                    color="#DE583D"
                    style={style.buttonbutton}
                    title='Register'
                    onPress={goToRegister}
                    />
            </View>
        </View>
    )
}
const style = StyleSheet.create({
    text:{
        fontSize: 40,
        paddingTop: 20,
        textAlign: 'center'
    },
    button:{
        textAlign: 'center'
    },
    buttonbutton:{
        textAlign: 'center',
        alignItems: 'center',
        fontWeight: 'bold'
    },
    hello: {
        color: '#fff',
        fontSize: 30,
        backgroundColor: '#4B19C8',
        textAlign: 'center',
    },
    view:{
        backgroundColor: '#DE583D',
        width: '50%',
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#fff',
        borderWidth: 1
    },
    viewtwo:{
        backgroundColor: '#fff',
        width: '50%',
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#6FD9FB',
        borderWidth:1
    }
})