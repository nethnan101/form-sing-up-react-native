import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { ButtonAdd } from './Button';

export const Homepage = () => {
    return (  
        <View style={style.container}>
            <Text style={style.text}>Welcom to our App!</Text>
            <ButtonAdd />
        </View>
    )
}
const style = StyleSheet.create({
    container:{
        width: '100%',
        height: '100%',
        backgroundColor: '#4893C8',
    },
    text:{
        paddingTop: '50%',
        fontSize:30,
        alignContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        color: '#fff',
        fontWeight: 'bold',
    }
})
